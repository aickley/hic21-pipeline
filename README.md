## hic21-pipeline

Running HiC-Pro on
[hic21-samples](https://bitbucket.org/aickley/hic21-samples/)
using reference (indices) built by
[hic21-references](https://bitbucket.org/aickley/hic21-references/)
in a bootstrapped conda environment
(part of [hic21](https://bitbucket.org/aickley/hic21-meta/) pipeline).

### Usage

Command `make runs` will create HiC-Pro job submission scripts in
`runs/...` that should be run via `bsub < script` in the respective
directories.

### Details

[Makefile](src/master/Makefile) has following targets:

  *  `conda`: installs [miniconda2](https://conda.io/miniconda.html) in `/deps/conda`.
  *  `hicpro_env`: creates a same-named conda environment and installs the dependencies of
      [HiC-Pro](https://github.com/nservant/HiC-Pro).
  *  `hicpro`: installs [HiC-Pro](https://github.com/nservant/HiC-Pro) in
      `deps/HiC_Pro_$VERSION`. The install config is defined in the `Makefile`
      (`HICPRO_INSTALL_CONFIG`) and the dependencies are:
      *  Python with necessary libs: provided by `hicpro_env`
      *  bowtie2, R: the ones installed globally in the cluster
  *  `fragments.bed`: generates restriction fragments from references.
  *  `gen/hicpro-run-{twins,unrel}`: generates HiC-Pro run configs from
      `cfg/hicpro-run.templ`. The one for `twins` uses the reference genome
      with respective SNPs.
  *  `runs/{production,truncated}/{twins,unrel}`: checks that the repo has no
      uncommitted changes generates HiC-Pro job submission script in the
      respective subdirectory (e.g. `runs/production/twins/$CURRENT_COMMIT`).
      Also creates a file `run-info` there with my contact details and
      repository/commit details.
  *  `runs`: creates run scripts for all run configurations.



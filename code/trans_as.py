import pysam
import numpy
import logging
import argparse
import os
import sys
import time
from pathlib import Path
from collections import defaultdict

#import chrlabels
import ordering
import progress_tracking


def default_logging_cfg():
    return dict(
        format='%(asctime)s %(levelname)-4s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('bamfile', type=argparse.FileType('r'),
                        help='path of the BAM file with XA allelic tags')
    parser.add_argument('-o', '--output-dir', required=True,
                        help='output directory')
    parser.add_argument('-q', '--quality', type=int, default=30,
                        help='minimum read quality')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='activate verbose logging')
    parser.add_argument('-f', '--log-freq', type=float, default=60,
                        help='log progress stats every n seconds')
    parser.add_argument('-m', '--max-reads', type=int, default=None,
                        help='maximum number of read pairs to process')
    return parser.parse_args()

def process_args(logging_cfg):
    args = parse_args()
    if args.verbose:
        logging_cfg['level'] = logging.INFO
    return logging_cfg, args

def log_progress(stats):
    stats['pct_saved'] = stats['saved'] / max(stats['read'], 1) * 100
    msg = 'read {read:d}, saved {saved:d} pairs ({pct_saved:.8f}%)'.format(**stats)
    logging.info(msg)
    logging.getLogger().handlers[0].flush()

def output_filename_base(s):
    return Path(s).stem.split('.')[0]
    #parts = stem.split('_')
    #return '_'.join(parts[:2]+[parts[-3]])

logging_cfg, args = process_args(default_logging_cfg())
output_dir = Path(args.output_dir)
output_dir.mkdir(exist_ok=True)
#logging_cfg['filename'] = str(output_dir / (output_filename_base(args.bamfile.name) + '.log'))

logging.basicConfig(**logging_cfg)
logging.info('Command line arguments: ' + ' '.join(sys.argv[1:]))
logging.info('Input file: %s' % args.bamfile.name)

sam_handle = pysam.AlignmentFile(args.bamfile)
input_progress = progress_tracking.InputFileProgress(args.bamfile)
references = list(sam_handle.header.references)

contacts = defaultdict(int)
stats = defaultdict(int)
stats['lastlog'] = time.time()
any_unmapped = lambda r1, r2: r1.is_unmapped or r2.is_unmapped
bad_quality = lambda r1, r2: min(r1.mapping_quality, r2.mapping_quality) < args.quality

readiter = sam_handle.fetch(until_eof=True)

while readiter:
    try:
        r1 = next(readiter)
        r2 = next(readiter)
    except StopIteration:
        break
    stats['read'] += 1
    if stats['read'] % 100000 == 0:
        time_secs = time.time()
        since_last_log = time_secs - stats['last_log']
        if since_last_log > args.log_freq:
            stats['last_log'] = time_secs
            log_progress(stats)
            logging.info(input_progress.report())
    if args.max_reads and stats['read'] >= args.max_reads:
        break
    if r1.query_name != r2.query_name:
        logging.error('Non matching QNAMEs at position {:d}:\n{:s}\n{:s}'.format(
                      status['pairs_read']*2, r1.query_name, r2.query_name))
        sys.exit(-2)
    #if ((not r1.is_unmapped and r1.mapping_quality < args.quality) or
    #    (not r2.is_unmapped and r2.mapping_quality < args.quality)):
    #    continue
    # Id in the BAM File. -1 indicates unmapped in BAM
    r1id = r1.reference_id+1
    r2id = r2.reference_id+1
    # Allele information (0: none, 1: first, 2:second)
    r1a = r1.get_tag('XA') if r1id else 0
    r2a = r2.get_tag('XA') if r2id else 0
    if max(r1a, r2a) > 2:
        logging.error('Conflicting allele (XA=3) at position {:d}:\n{:s}\n{:s}'.format(
                      stats['read']*2, r1.query_name, r2.query_name))
        continue
    contacts[ordering.ordered(r1id, r2id, r1a, r2a)] += 1
    stats['saved'] += 1

logging.info('Exited processing loop...')
log_progress(stats)

logging.info('Closing input file...')
sam_handle.close()

import pandas
contacts = pandas.Series(contacts)
contacts.index.set_names(['ref1', 'ref2', 'al1', 'al2'], inplace=True)

# Mapping from adjusted ids where 0 refers to no alignment
mapping = dict(enumerate(['-'] + references))
ref1labels = [mapping[i] for i in contacts.index.levels[0]]
ref2labels = [mapping[i] for i in contacts.index.levels[1]]
al1labels = contacts.index.levels[2]
al2labels = contacts.index.levels[3]
contacts.index.set_levels([ref1labels, ref2labels, al1labels, al2labels], inplace=True)

logging.info(str(output_dir))
logging.info(args.bamfile.name)
output_file = str(output_dir / output_filename_base(args.bamfile.name))
logging.info('Writing results to {:s}.{{csv,pickle}}'.format(output_file))
contacts.to_csv(output_file + '.csv', header=True)
contacts.to_pickle(output_file + '.pickle')

logging.info('All done. Terminating...')



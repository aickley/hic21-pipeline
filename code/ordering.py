#!/usr/bin/env python3

def ordered(ref1, ref2, al1, al2):
    if ref1 == ref2:
       return (ref1, ref2, al1, al2) if al1 < al2 else (ref1, ref2, al2, al1)
    else:
       sr = ref1 > ref2      # swap refs?
       sa = sr and (al1 != 0) or not sr and (al2 ==0)  # swap alleles?
       return (
           ref1 if not sr else ref2,
           ref1 if sr else ref2,
           al1 if not sa else al2,
           al1 if sa else al2)

if __name__ == '__main__':

   import unittest

   runner = unittest.TextTestRunner()
   test = lambda f: runner.run(unittest.FunctionTestCase(f))

   @test
   def test_chr_unmapped():
       # When one read is unmapped
       # - it is always put first
       # - the allelic index of the other read is put last
       assert ordered(1, 0, 0, 0) == (0, 1, 0, 0)
       assert ordered(1, 0, 1, 0) == (0, 1, 0, 1)
       assert ordered(0, 1, 0, 1) == (0, 1, 0, 1)

   @test
   def test_same_chr():
       assert ordered(1, 1, 1, 0) == (1, 1, 0, 1)
       assert ordered(1, 1, 0, 1) == (1, 1, 0, 1)
       assert ordered(1, 1, 1, 2) == (1, 1, 1, 2)
       assert ordered(1, 1, 2, 1) == (1, 1, 1, 2)

   @test
   def test2():
       assert ordered(1, 2, 1, 0) == (1, 2, 0, 1)
       assert ordered(1, 2, 0, 1) == (1, 2, 0, 1)
       assert ordered(1, 2, 1, 2) == (1, 2, 1, 2)
       assert ordered(1, 2, 2, 1) == (1, 2, 2, 1)

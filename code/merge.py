import argparse
import pandas as pd
import functools
from pathlib import Path


def parse_args():
    parser = argparse.ArgumentParser(description='Merge contact counts.')
    parser.add_argument('out')
    parser.add_argument('files',
                        metavar='files',
                        nargs='+',
                        #type=argparse.FileType('r'),
                        help='pickle files with contact count series')
    return parser.parse_args()

def to_cat_index(s):
    make = lambda i: pd.Categorical(s.index.get_level_values(i), s.index.levels[i], ordered=True)
    indices = [make(i) for i in range(len(s.index.levels))]
    s.index = pd.MultiIndex.from_arrays(indices, names=s.index.names)
    s.name = 'counts'
    return s

def get_sample_metadata(path):
    stem = Path(path).stem
    src, tag, lane, _ = stem.split('_')
    lane = int(lane.strip('L'))
    return src, tag, lane

def unstack(merged):
    return merged.unstack(['ref2', 'al2']).fillna(0).astype(int)

def main():
    args = parse_args()
    series = map(to_cat_index, map(pd.read_pickle, args.files))
    names = ['ref1', 'ref2', 'al1', 'al2']
    f = lambda x,y: pd.merge(x.reset_index(), y.reset_index(), how='outer', on=names).set_index(names)
    merged = functools.reduce(f, series).fillna(0).astype(int)
    merged.columns = pd.MultiIndex.from_tuples(list(map(get_sample_metadata, args.files)), names=['src', 'tag', 'lane'])
    #table = unstack(merged)
    merged.to_pickle('tmp2/' + args.out + '.pickle')
    #merged.to_csv('tmp2/' + args.out + '.csv')
    #table.to_csv('tmp2/' + args.out + '_table.csv')
    return merged

if __name__ == '__main__':
    merged = main()


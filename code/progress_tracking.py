import time
import os

from scientific_notation import format_binary


class PeriodicTrigger:

    def __init__(self, value, period):
        self.value = value
        self.period = period
        self._triggered_at = value

    def update_with(self, value, reset=False):
        self.value = value
        if self.value > self._triggered_at + self.period:
            if self.reset:
                self._triggered_at = self.value
            return True
        else:
            return False

    def update_by(self, increment, reset=False):
        return self.update_with(self.value + increment)


class InputFileProgress:

    def __init__(self, file):
        self.started_at = time.time()
        self.file = file
        self.pos = file.tell()
        self.size = os.fstat(file.fileno()).st_size

    def update(self):
        self.pos = self.file.tell()
        seconds_spent = time.time() - self.started_at
        self.bytes_per_second = int(self.pos / seconds_spent)

    def report(self, update=True):
        if update:
            self.update()
        bytes_per_minute = self.bytes_per_second * 60
        minutes_per_gigabyte = 1/(bytes_per_minute / 1024**3)
        return 'Processed {:s} of {:s} ({:.1f}m/G average {:.1f}m remaining)'.format(
            format_binary(self.pos),
            format_binary(self.size),
            minutes_per_gigabyte,
            (self.size - self.pos) / bytes_per_minute)
